# Shoot-Dwan

1. Execute `create-configs.sh`
2. Put your bot token in the generated `.env` file
3. Fill out the generated `config.json`
   - `monitored` is a list of discord ids of the gremlins you want this bot to shoot
   - `TTL` is the amount of time a message is consididered a repost in milliseconds
     - If the TTL is 3000 any similar message posted within 3 seconds will be removed
     - TTL has a minimum value of 3000 (3 seconds)
4. Run `docker-compose up`
5. Profit

P.S. you can change the values in `config.json` without restarting the container and the changes will be applied immediately
