const Keyv = require("@keyv/redis");
const { getTTL } = require("./configreader");

const keyv = new Keyv(`redis://user@redis:6379`);
const cdnRegex = /https:\/\/cdn.discordapp.com\/emojis\/(?<id>\d+).*/g;
const emojiRegex = /<a?:.+?:(?<id>\d+?)>/g;

/**
 * Returns any combination of the message content, embeded links,
 * emote ids and id of an emote hosted on the discord cdn
 * if found in the message
 */
function getRepostableContent(msg) {
  return new Set(
    [
      msg.content,
      ...msg.embeds.map((e) => e.url),
      ...Array.from(msg.content.matchAll(cdnRegex), (arr) => arr[1]),
      ...Array.from(msg.content.matchAll(emojiRegex), (arr) => arr[1]),
    ].filter((truthy) => truthy),
  );
}

async function isRepost(content) {
  return await keyv.get(content);
}

exports.storeContent = async function (msg) {
  for (const item of getRepostableContent(msg)) {
    await keyv.set(item, true, await getTTL());
  }
};

exports.checkMessage = async function (msg) {
  for (const item of getRepostableContent(msg))
    if (await isRepost(item)) {
      console.info("Removing repost from " + msg.author.username);
      await msg.delete();
      return;
    }
};
