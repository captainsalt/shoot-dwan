const { Client, Intents } = require("discord.js");
const { checkMessage, storeContent } = require("./repostchecker");
const { isMonitoredUser } = require("./configreader");

const client = new Client({
  intents: [Intents.FLAGS.GUILD_MESSAGES, Intents.FLAGS.GUILDS],
});

client.on("ready", () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on("messageCreate", async (msg) => {
  if (await isMonitoredUser(msg.author.id)) await checkMessage(msg);
  await storeContent(msg);
});

client.login(process.env.DISCORD_TOKEN);
