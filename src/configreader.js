const fs = require("fs/promises");
const path = require("path");

async function readConfig() {
  try {
    const configPath = path.join(__dirname, "..", "config.json");
    const content = await fs.readFile(configPath, { encoding: "utf-8" });

    return JSON.parse(content);
  } catch (error) {
    console.error("Could not read config.json. Please make sure it's correct");
  }
}

exports.isMonitoredUser = async function (id) {
  const monitoredList = (await readConfig())?.monitored || [];

  return [id, +id]
    .map((id) => monitoredList.includes(id))
    .some((isTrue) => isTrue);
};

exports.getTTL = async function () {
  const minimum = 3_000;
  const ttl = (await readConfig())?.TTL;

  if (!Number.isInteger(ttl) || ttl < minimum) return minimum;

  return ttl;
};
